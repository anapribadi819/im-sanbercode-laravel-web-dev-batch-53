<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas 9 – OOP PHP</title>
</head>
<body>
    <h1>Tugas 9 - OOP </h1>
    <?php
        require_once("animal.php");
        require_once("frog.php");
        require_once("ape.php");

        
        echo "<h2>Release 0</h2>";

        $sheep = new Animal("shaun");

        echo $sheep->name; // "shaun"
        echo "<br>";
        echo $sheep->legs; // 4
        echo "<br>";
        echo $sheep->cold_blooded; // "no"
        echo "<br>";

        // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

        echo "<h2>Release 1</h2>";

        // index.php
        $sungokong = new Ape("kera sakti");
        $sungokong->yell() ; // "Auooo"
        echo "<br>";
        $kodok = new Frog("buduk");
        $kodok->jump() ; // "hop hop"

        echo "<h2>Output Akhir</h2>";

        echo "Name: " . $sheep->name . "<br>";
        echo "legs: " . $sheep->legs . "<br>";
        echo "cold blooded: " . $sheep->cold_blooded . "<br><br>";

        echo "Name: " . $kodok->name . "<br>";
        echo "legs: " . $kodok->legs . "<br>";
        echo "cold blooded: " . $kodok->cold_blooded . "<br>";
        echo "Jump : ";
        echo $kodok->jump() . "<br><br>";

        echo "Name: " . $sungokong->name . "<br>";
        echo "legs: " . $sungokong->legs . "<br>";
        echo "cold blooded: " . $sungokong->cold_blooded . "<br>";
        echo "Yell : ";
        echo $sungokong->yell() . "<br>";

    ?>
</body>
</html>