<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('pages.form');
    }

    public function welcome()
    {
        return view('pages.welcome');
    }

    public function send(request $request)
    {
        $fname = $request['firstname'];
        $lname = $request['lastname'];

        return view('pages.welcome', ['firstname'=> $fname, 'lastname'=> $lname]);
    }
}
