<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        return view('home');
    }

    public function table()
    {
        return view('pages.table');
    }

    public function data_tables()
    {
        return view('pages.data-tables');
    }

}