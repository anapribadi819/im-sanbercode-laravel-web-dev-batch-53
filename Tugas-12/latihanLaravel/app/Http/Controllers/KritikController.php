<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kritik;
use Illuminate\Support\Facades\Auth;

class KritikController extends Controller
{
    public function store($film_id, Request $request){
        $request->validate([
            'content' => 'required',
            'point' => 'required'
        ]);

        $userId = Auth::id();
        $kritik = new Kritik;
        $kritik->content= $request->content;
        $kritik->point= $request->point;
        $kritik->user_id= $userId;
        $kritik->film_id= $film_id;

        $kritik->save();    

        return redirect('/film/'.$film_id);
    }
}
