<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\KritikController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Route::get('/', [HomeController::class, 'home']);

Route::get('/table', [HomeController::class, 'table']);

Route::get('/data-tables', [HomeController::class, 'data_tables']);

Route::get('/register', [AuthController::class, 'register']);

Route::get('/welcome', [AuthController::class, 'welcome']);

Route::post('/send', [AuthController::class, 'send']);

Route::group(['middleware' => ['auth']], function () {

    // C -> Create Data
    // Menampilkan form untuk membuat data pemain film baru
    Route::get('/cast/create', [CastController::class, 'create']);

    
    // U -> Update
    // Menampilkan form untuk edit pemain film dengan id tertentu
    Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

    // CRUD Profile
    Route::resource('profile', ProfileController::class)->only(['index', 'update']);

    
    // Tambah Komentar
    Route::post('/kritik/{film_id}', [KritikController::class, 'store']);
    
});

// CRUD Cast

    // C -> Create Data
    // Menampilkan form untuk membuat data pemain film baru
    // Route::get('/cast/create', [CastController::class, 'create']);

    // Menyimpan data baru ke tabel Cast
    Route::post('/cast', [CastController::class, 'store']);

    // R -> Read Data
    // Menampilkan list data para pemain film
    Route::get('/cast', [CastController::class, 'index']);

    // Mmenampilkan detail data pemain film dengan id tertentu
    Route::get('/cast/{cast_id}', [CastController::class, 'show']);

    // U -> Update
    // Menampilkan form untuk edit pemain film dengan id tertentu
    // Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

    // Menyimpan perubahan data pemain film (update) untuk id tertentu
    Route::put('/cast/{cast_id}', [CastController::class, 'update']);

    // D -> Delete data
    // Menghapus data pemain film dengan id tertentu
    Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);

    // CRUD Post
    Route::resource('post', PostController::class);

    // CRUD Film
    Route::resource('film', FilmController::class);

    // CRUD Genre
    Route::resource('genre', GenreController::class);
    

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();
