@extends('layout.master')

@section('judul')
    Halaman Detail Genre
@endsection

@section('content')

    <div class="card-body">
        <h5>{{$genre->nama}}</h5>
        <br>
        <div class="row">
            @forelse ($genre->films as $item)
            <div class="col-4">
                <div class="card">
                    <img src="{{asset('image/'.$item->poster)}}" class="card-img-top" alt="">
                    <div class="card-body">
                        <h5>{{$item->judul}}</h5>
                         <p class="card-text">{{Str::limit($item->ringkasan, 100)}}</p>
                         <p class="card-text">Tahun: {{$item->tahun}}</p>
                        <a href="/film/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Read Me</a>
                    </div>
                </div>

            </div>
            @empty
                <h3>Kategori ini tidak ada postingan</h3>
            @endforelse
        </div>
       
        <br>
        <a href="/genre" class="btn btn-secondary btn-block btn-sm">Kembali</a>
    </div>

@endsection