@extends('layout.master')

@section('judul')
    Genre
@endsection

@section('content')
@auth
<a href="/genre/create" class="btn btn-primary btn-sm mb-3">Tambah</a>
@endauth
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Genre</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($genre as $key => $value)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$value->nama}}</td>
            <td>
                <form action="/genre/{{$value->id}}" method="POST">
                    @csrf
                    <a href="/genre/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                    @method('DELETE')
                    @auth
                    <a href="/genre/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    @endauth
                </form>
            </td>
        </tr>
      @empty
        <tr>
            <td>Tidak Ada Data </td>
        </tr>
      @endforelse
    </tbody>
  </table>
@endsection