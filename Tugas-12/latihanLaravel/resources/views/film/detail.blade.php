@extends('layout.master')

@section('judul')
    Halaman Detail Film
@endsection

@section('content')

    <img src="{{asset('image/'.$film->poster)}}" class="card-img-top" alt="">
    <div class="card-body">
        <h5>{{$film->judul}}</h5>
        <span class="badge badge-info"> {{$film->genre->nama}}</span>
         <p class="card-text">{{$film->ringkasan}}</p>
         <p class="card-text">Tahun: {{$film->tahun}}</p>
         {{-- <p class="card-text">Genre: {{$genre[($film['genre_id']-1)]->nama}}</p> --}}
        <a href="/film" class="btn btn-secondary btn-block btn-sm">Kembali</a>
    </div>

    <br>
    <h3>List Kritik</h3>
    <br>

    @forelse ($film->kritik as $item)
    <div class="card">
        <div class="card-header bg-dark">
          {{$item->user->name}}
        </div>
        <div class="card-body">
           <p class="card-text">{{$item->content}}</p>
           <p class="card-text">Point: {{$item->point}}</p>
        </div>
    </div>
    @empty
        <h3>Tidak ada kritik untuk film ini</h3>    
    @endforelse

    @auth
    <form action="/kritik/{{$film->id}}" method="POST">
        @csrf
        <div class="form-group">
            <textarea name="content" class="form-control" placeholder="Buat Kritik ..." @error('content') is-invalid @enderror" cols="30" rows="10"></textarea>
        </div>
        @error('content')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Point</label>
            <input type="number" name="point" class="form-control" placeholder="Beri point 1-10 ..." @error('point') is-invalid @enderror">
        </div>
        @error('point')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Tambah Kritik</button>
    </form>
    @endauth

@endsection