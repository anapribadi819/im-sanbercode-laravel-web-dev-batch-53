@extends('layout.master')

@section('judul')
    Halaman List Film
@endsection

@section('content')
@auth
<a href="/film/create" class="btn btn-primary btn-sm mb-4">Tambah Film</a>
@endauth
<div class="row">
    @forelse ($film as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{asset('image/'.$item->poster)}}" class="card-img-top" alt="">
                <div class="card-body">
                    <h5>{{$item->judul}}</h5>
                    <span class="badge badge-info"> {{$item->genre->nama}}</span>
                     <p class="card-text">{{Str::limit($item->ringkasan, 100)}}</p>
                     <p class="card-text">Tahun: {{$item->tahun}}</p>
                     {{-- <p class="card-text">Genre: {{$item->genre->nama}}</p> --}}
                    <a href="/film/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Read Me</a>
                    @auth
                        <div class="row my-2">
                            <div class="col">
                                <a href="/film/{{$item->id}}/edit" class="btn btn-info btn-block btn-sm">Edit</a>

                            </div>
                            <div class="col">
                                <form action="/film/{{$item->id}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">
                                </form>
                            </div>
                        </div>
                    @endauth
                </div>
            </div>

        </div>
    @empty
        <h2>Tidak ada postingan</h2>
    @endforelse

</div>

@endsection