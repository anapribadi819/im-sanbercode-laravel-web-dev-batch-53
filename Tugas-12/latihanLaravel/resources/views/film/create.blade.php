@extends('layout.master')

@section('judul')
    Halaman Tambah Film
@endsection

@section('content')
    <form action="/film" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>Nama Film</label>
            <input type="text" name="judul" class="form-control @error('judul') is-invalid @enderror">
        </div>
        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Ringkasan</label>
            <textarea name="ringkasan" class="form-control @error('ringkasan') is-invalid @enderror" cols="30" rows="10"></textarea>
        </div>
        @error('ringkasan')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Tahun</label>
            <input type="number" name="tahun" class="form-control @error('tahun') is-invalid @enderror">
        </div>
        @error('tahun')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Poster</label>
            <input type="file" name="poster" class="form-control @error('poster') is-invalid @enderror">
        </div>
        @error('poster')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Genre</label>
            <select name="genre_id" id="" class="form-control">
                <option value="">-- Pilih Kategori --</option>
                @forelse ($genre as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @empty
                    <option value="">Tidak ada Data Genre</option>
                @endforelse
            </select>
        </div>
        @error('tahun')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
       
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection