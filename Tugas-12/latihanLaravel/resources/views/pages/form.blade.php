@extends('layout.master')

@section('judul')
Halaman Pendaftaran
@endsection

@section('content')

    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/send" method="post">
        @csrf
        <label for="firstname">First name:</label> <br>
        <input type="text" name="firstname" id="firstname"> <br> <br>
        <label for="lastname">Last name:</label> <br>
        <input type="text" name="lastname" id="lastname"> <br> <br>
        <label>Gender:</label> <br>
        <input type="radio" name="gender" id="man" value="man">
        <label for="man"> Man </label> <br>
        <input type="radio" name="gender" id="woman" value="woman">
        <label for="woman"> Woman </label> <br>
        <input type="radio" name="gender" id="otherGender" value="otherGender">
        <label for="otherGender"> Other </label> <br> <br>
        <label>Nationality:</label>
        <select name="nationality"> 
            <option value="indonesian">Indonesian</option> 
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option> 
            <option value="australian">Australian</option>
        </select>
        <br> <br>
        <label>Language Spoken:</label> <br> 
        <input type="checkbox" name="language" id="indonesia" value="indonesia">
        <label for="indonesia">Bahasa Indonesia</label> <br>
        <input type="checkbox" name="language" id="english" value="english">
        <label for="english">English</label><br>
        <input type="checkbox" name="language" id="arabic" value="arabic">
        <label for="arabic">Arabic</label> <br>
        <input type="checkbox" name="language" id="japanese" value="japanese">
        <label for="japanese">Japanese</label> <br>
        <br>
        <label>Bio:</label> <br>
        <textarea name="bio" rows="10" cols="30"></textarea> <br> <br>
        <input type="submit" value="Sign Up">
    </form>
@endsection