@extends('layout.master')

@section('judul')
    Halaman Edit Kategori
@endsection

@section('content')
    <form action="/cast/{{$castById->id}}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>Nama Cast</label>
            <input type="text" name="nama" value="{{$castById->nama}}" class="form-control @error('nama') is-invalid @enderror">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Umur</label>
            <input type="number" name="umur" value="{{$castById->umur}}" class="form-control @error('umur') is-invalid @enderror">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Bio</label>
            <textarea name="bio" class="form-control @error('bio') is-invalid @enderror" cols="30" rows="10">{{$castById->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection