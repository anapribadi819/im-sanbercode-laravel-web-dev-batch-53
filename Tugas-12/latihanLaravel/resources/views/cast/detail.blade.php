@extends('layout.master')

@section('judul')
    Detail Cast
@endsection

@section('content')

<h1>{{$castById->nama}}</h1>
<p>{{$castById->umur}} tahun</p>
<p>{{$castById->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>

@endsection